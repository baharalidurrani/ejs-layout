$(document).ready(function () {
    $('.sidenav').sidenav();
    $('.dropdown-trigger').dropdown();
    $('.collapsible').collapsible();
    $('.modal').modal();
    if (typeof (Storage) !== "undefined") {
        var colorArgs;
        if (colorArgs = localStorage.getItem("lastTheme"))
            changeTheme(colorArgs);
    } else {
        console.log("localstorage not available");
    }
});

function changeTheme(colorArgs) {
    $('.sidenav').sidenav('close');

    //runtime mobile statusbar change
    document.getElementsByTagName("meta")[0].setAttribute("content", colorArgs);

    //change primary theme (CSS variable)
    $('body').css('--primary', colorArgs);

    //save primary theme for next time
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem("lastTheme", colorArgs);
        if (colorArgs === '#ee6e73')
            localStorage.removeItem('lastTheme');
    } else {
        console.log("localstorage not available");
    }

    //for changing backround gradient
    var color = colorArgs.replace('#', 'g');
    $("#root").attr('class', color);
}