const express = require('express');
const Router = express.Router();

const Login = require('../Controllers/LoginController');
const Register = require('../Controllers/RegisterController');
const Home = require('../Controllers/HomeController');
const Profile = require('../Controllers/ProfileController');
const Logout = require('../Controllers/LogoutController');
const Repo = require('../Controllers/RepoController');
const Paper = require('../Controllers/PaperController');

module.exports = Router;

//Home route
Router.get('/', Home);

//Login Route
Router.get('/login', Login.get);
Router.post('/login', Login.post);

Router.get('/register', Register.get);

Router.get('/profile', Profile.display);

Router.get('/logout', Logout);

Router.get('/repo', Repo.get);

Router.get('/paper', Paper.get);