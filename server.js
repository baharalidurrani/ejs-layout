const express = require('express');
const body_parser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;

//server listing on port 3000
app.listen(port, () => {
    console.log(`server listing on port ${port}`);
});

//setting template engine
app.set('view engine', 'ejs');

//static files
app.use(express.static('./views/assets'));
// app.use('/css', express.static(__dirname + ('/assets/css')));
// app.use('/js', express.static(__dirname + ('assets/')));
// app.use('/img', express.static(__dirname + ('./views/assets')));
// app.use('/pdf', express.static(__dirname + ('./views/assets/pdf')));
//app.use('/assets', express.static(path.join(__dirname, './views/assets')))

//middleware
app.use(body_parser.urlencoded({
    extended: false
}));

//fetch routes
app.use(require('./Routes/Routes'));